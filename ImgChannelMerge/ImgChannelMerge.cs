﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using OpenCvSharp;

using libBase;

namespace ImgChannelMerge
{

    public class ImgChannelMerge : libBase.BaseModule<Mat, Mat>
    {
        int stride = 1;
        int kernel_size = 3;
        int threshold = 0;

        public ImgChannelMerge()
        {
            Info.Category = libBase.FunctionCategory.All;
            Info.Title = "Mask Blurring";
            Info.Description = "마스크 이미지를 이용한 이미지 흐림";
            

        }
        // [TypeConverter(typeof(libBase.Common.brEnumConvertor))] --> enum text convertor
        [Description("Blur Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TBlurType blur_type
        {
            get
            {
                return blur_type_;
            }
            set
            {
                blur_type_ = value;
                Action();
            }
        }
        [Description("Stride")]
        public int Stride { get { return stride; } set { stride = value; Action(); } }
        [Description("Kernel Size")]
        public int Kernel_size { get { return kernel_size; } set { kernel_size = value; Action(); } }
        [Description("Threshold")]
        public int Threshold { get { return threshold; } set { threshold = value; Action(); } }
        
        protected override Mat Execute(Mat value)
        {
            if (value == null)
                return null;
            IntPtr src = value.Data;
            int bw = value.Width;
            int bh = value.Height;
            IntPtr msk = NewImg(bw, bh, TPixFmtCs.Gray8);
            IntPtr dst = NewImg(bw, bh, TPixFmtCs.Gray8);

            //int blur_type_;
            CppSrc.BlurringUseMask(src, msk, dst, value.Width, value.Height, (int)blur_type_, kernel_size, stride, threshold);

            Mat result = new Mat(bh, bw, MatType.CV_8UC1, dst);
            DelImg(msk);

            return result;
        }
    }
}
