﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Hims.hLibCm;
using OpenCvSharp;

using libBase;

namespace IA_Blurring_HSV
{
    public partial class CppSrc
    {
        [DllImport("Blurring.dll")]
        public static extern void BlurringUseMask(IntPtr src, IntPtr msk, IntPtr dst, int bw, int bh, int blur_type, int kernel_size, int stride, int threshold);
    }
    public enum TPixFmtCs { Gray8, Rgb32, Num }

    public enum TBlurType
    {
        [Description("Average")]
        BLUR_AVG,
        [Description("Max")]
        BLUR_MAX,
        [Description("Stacdard Devation")]
        BLUR_STDDEV,
        [Description("Avg + Stddev")]
        BLUR_AVG_PLUS_STDDEV,
    }

    public class HSVBlur_usingMaskImage : libBase.BaseModule<Mat, Mat>
    {
        TBlurType blur_type_ = TBlurType.BLUR_AVG;
        int stride = 1;
        int kernel_size = 3;
        int threshold = 0;
        int h_hsv = 0;
        int s_hsv = 0;

        public HSVBlur_usingMaskImage()
        {
            Info.Category = libBase.FunctionCategory.All;
            Info.Title = "Mask Blurring HSV";
            Info.Description = "마스크 이미지를 이용한 이미지 흐림(HSV)";


        }
        //public override int NoOfInputs { get { return 2; } }
        // [TypeConverter(typeof(libBase.Common.brEnumConvertor))] --> enum text convertor
        [Description("Blur Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TBlurType blur_type
        {
            get
            {
                return blur_type_;
            }
            set
            {
                blur_type_ = value;
                Action();
            }
        }
        [Description("Stride")]
        public int Stride { get { return stride; } set { stride = value; Action(); } }
        [Description("Kernel Size")]
        public int Kernel_size { get { return kernel_size; } set { kernel_size = value; Action(); } }
        [Description("Threshold")]
        public int Threshold { get { return threshold; } set { threshold = value; Action(); } }
        [Description("H(HSV)")]
        public int H_hsv { get { return h_hsv; } set { h_hsv = value; Action(); } }
        [Description("S(HSV)")]
        public int S_hsv { get { return s_hsv; } set { s_hsv = value; Action(); } }


        protected override Mat Execute(Mat value)
        {
            if (value == null)
                return null;
            Mat src_hsv = new Mat();
            Cv2.CvtColor(value, src_hsv, ColorConversionCodes.RGB2HSV);
            Mat[] src_hsvs = Cv2.Split(src_hsv);
            Mat src_v = src_hsvs[2];

            IntPtr src = src_v.Data;
            int bw = value.Width;
            int bh = value.Height;
            IntPtr msk = NewImg(bw, bh, TPixFmtCs.Gray8);
            IntPtr dst = NewImg(bw, bh, TPixFmtCs.Gray8);

            //int blur_type_;
            CppSrc.BlurringUseMask(src, msk, dst, value.Width, value.Height, (int)blur_type_, kernel_size, stride, threshold);

            Mat Mdst = new Mat(bh, bw, MatType.CV_8UC1, dst);
            DelImg(msk);
            Mat result = new Mat();
            src_hsvs[0] = src_hsvs[0] + h_hsv;
            src_hsvs[1] = src_hsvs[1] + s_hsv;
            src_hsvs[2] = Mdst.Clone();
            Cv2.Merge(src_hsvs, result);
            Cv2.CvtColor(result, result, ColorConversionCodes.HSV2RGB);

            return result;
        }




        public static int GetBytePP(TPixFmtCs pf)
        {
            int bytepp = 1;
            if (pf == TPixFmtCs.Gray8) bytepp = 1;
            if (pf == TPixFmtCs.Rgb32) bytepp = 4;
            return bytepp;
        }
        public static IntPtr NewImg(int bw, int bh, TPixFmtCs pf)
        {
            //IntPtr buf = hlib.New64_A16(bw * bh * GetBytePP(pf));
            IntPtr buf = hlib.New32(bw * bh * GetBytePP(pf));
            if (buf == IntPtr.Zero) return IntPtr.Zero;
            hlib.MemSet(buf, 0, (long)bw * bh * GetBytePP(pf));
            return buf;
        }
        public static void DelImg(IntPtr buf)
        {
            hlib.Delete32(buf);
        }

    }
}
