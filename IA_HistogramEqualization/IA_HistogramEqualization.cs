﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using OpenCvSharp;

using libBase;

namespace IA_HistogramEqualization
{
    public enum TEqualizationType
    {
        [Description("Normal")]
        EQUALIZE_NORMAL,
        [Description("CLAHE")]
        EQUALIZE_CLAHE,
    }
    public enum TColorType
    {
        [Description("HSV")]
        COLOR_HSV,
        [Description("YCrCb")]
        COLOR_YCRCB,
        [Description("Lab")]
        COLOR_LAB,
        [Description("Luv")]
        COLOR_LUV,
        [Description("Yuv")]
        COLOR_YUV,
    }
    public class IA_HistogramEqualization : libBase.BaseModule<Mat, Mat>
    {
        TEqualizationType _equaliztion_type;
        double clipLimit = 4.0;
        int gridWidth = 8;
        int gridHeight = 8;
        public IA_HistogramEqualization()
        {
            Info.Category = libBase.FunctionCategory.All;
            Info.Title = "Histogram Equalization";
            Info.Description = "히스토그램 평준화";
        }
        [Description("Equalization Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TEqualizationType equaliztion_type
        {
            get
            {
                return _equaliztion_type;
            }
            set
            {
                _equaliztion_type = value;
                Action();
            }
        }
        [Description("Clip Limit")]
        public double _clipLimit { get { return clipLimit; } set { clipLimit = value; } }
        [Description("Grid Width")]
        public int _gridWidth { get { return gridWidth; } set { gridWidth = value; } }
        [Description("Grid Height")]
        public int _gridHeight { get { return gridHeight; } set { gridHeight = value; } } 

        protected override Mat Execute(Mat value)
        {
            if (value == null)
                return null;
            Size tileGridSize;
            tileGridSize.Width = gridWidth;
            tileGridSize.Height = gridHeight;
            Mat result = new Mat();

            switch (equaliztion_type)
            {
                case TEqualizationType.EQUALIZE_NORMAL:
                    Cv2.EqualizeHist(value, result);
                    break;
                case TEqualizationType.EQUALIZE_CLAHE:
                    var clahe = Cv2.CreateCLAHE(clipLimit, tileGridSize);
                    clahe.Apply(value, result);
                    break;
                default:
                    Cv2.EqualizeHist(value, result);
                    break;
            }

            return result;
        }

    }
    public class IA_HistogramEqualization_HSV : libBase.BaseModule<Mat, Mat>
    {
        TEqualizationType equaliztion_type;
        TColorType color_type;
        double clipLimit = 4.0;
        int gridWidth = 8;
        int gridHeight = 8;
        public IA_HistogramEqualization_HSV()
        {
            Info.Category = libBase.FunctionCategory.All;
            Info.Title = "Histogram Equalization (Color)";
            Info.Description = "칼라 이미지 히스토그램 평준화";
        }
        [Description("Equalization Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TEqualizationType _equaliztion_type { get { return equaliztion_type; } set { equaliztion_type = value; Action(); } }
        [Description("Color Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TColorType _color_type { get { return color_type; } set { color_type = value; Action(); } }
        [Description("Clip Limit")]
        public double _clipLimit { get { return clipLimit; } set { clipLimit = value; } }
        [Description("Grid Width")]
        public int _gridWidth { get { return gridWidth; } set { gridWidth = value; } }
        [Description("Grid Height")]
        public int _gridHeight { get { return gridHeight; } set { gridHeight = value; } }

        protected override Mat Execute(Mat value)
        {
            if (value == null)
                return null;
            Size tileGridSize;
            tileGridSize.Width = gridWidth;
            tileGridSize.Height = gridHeight;

            Mat src = new Mat();
            Mat result = new Mat();
            Mat dst = new Mat();
            Mat src_gray = new Mat();
            Mat[] src_ch;
            switch (color_type)
            {
                case TColorType.COLOR_HSV:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2HSV);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[2];
                    break;
                case TColorType.COLOR_YCRCB:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2YCrCb);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[0];
                    break;
                case TColorType.COLOR_LAB:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2Lab);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[0];
                    break;
                case TColorType.COLOR_LUV:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2Luv);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[0];
                    break;
                case TColorType.COLOR_YUV:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2YUV);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[0];
                    break;
                default:
                    Cv2.CvtColor(value, src, ColorConversionCodes.RGB2HSV);
                    src_ch = Cv2.Split(src);
                    src_gray = src_ch[2];
                    break;
            }
            
            switch (equaliztion_type)
            {
                case TEqualizationType.EQUALIZE_NORMAL:
                    Cv2.EqualizeHist(src_gray, dst);
                    break;
                case TEqualizationType.EQUALIZE_CLAHE:
                    var clahe = Cv2.CreateCLAHE(clipLimit, tileGridSize);
                    clahe.Apply(src_gray, dst);
                    break;
                default:
                    Cv2.EqualizeHist(src_gray, dst);
                    break;
            }

            switch (color_type)
            {
                case TColorType.COLOR_HSV:
                    src_ch[2] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.HSV2RGB);
                    break;
                case TColorType.COLOR_YCRCB:
                    src_ch[0] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.YCrCb2RGB);
                    break;
                case TColorType.COLOR_LAB:
                    src_ch[0] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.Lab2RGB);
                    break;
                case TColorType.COLOR_LUV:
                    src_ch[0] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.Luv2RGB);
                    break;
                case TColorType.COLOR_YUV:
                    src_ch[0] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.YUV2RGB);
                    break;
                default:
                    src_ch[2] = dst.Clone();
                    Cv2.Merge(src_ch, result);
                    Cv2.CvtColor(result, result, ColorConversionCodes.HSV2RGB);
                    break;
            }
            return result;
        }

    }
}