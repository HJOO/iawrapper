﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Hims.hLibCm;
using OpenCvSharp;


namespace AI2IAText
{

    public partial class CppSrc
    {
        [DllImport("Blurring.dll")]
        public static extern void BlurringUseMask(IntPtr src, IntPtr msk, IntPtr dst, int bw, int bh, int blur_type, int kernel_size, int stride, int threshold);
    }

    public enum TPixFmtCs { Gray8, Rgb32, Num }

    class Program
    {
        public enum TBlurType
        {
            BLUR_AVG,
            BLUR_MAX,
            BLUR_STDDEV,
            BLUR_AVG_PLUS_STDDEV,
        }
        static void Main(string[] args)
        {
            Mat _src = new Mat();
            _src = Cv2.ImRead("G:\\image\\g\\x20nwb01\\1F1B622QH1A001_0000_AQ_20181207113408.jpg", ImreadModes.Grayscale);
            Cv2.ImShow("src", _src);
            Mat _dst = new Mat();
            _dst = Program.Execute(_src);
            Cv2.ImShow("dst", _dst);
            Cv2.WaitKey();
            
            
        }
        private static Mat Execute(Mat _src)
        {
            if (_src == null)
                return null;
            Mat _dst;
            int bw = _src.Width;
            int bh = _src.Height;
            int kernel_size = 21;
            int stride = 1;
            int threshold = 120;
            TBlurType blur_type_ = TBlurType.BLUR_AVG;
            unsafe
            {
                IntPtr src = _src.Data;
                IntPtr msk = NewImg(bw, bh, TPixFmtCs.Gray8);
                IntPtr dst = NewImg(bw, bh, TPixFmtCs.Gray8);
                //IntPtr dst = IntPtr.Zero;
                //IntPtr msk = IntPtr.Zero;
                //byte[] bdst = new byte[bw * bh];
                //byte[] bmsk = new byte[bw * bh];
                //Marshal.Copy(dst, bdst, 0, bw * bh);
                //Marshal.Copy(msk, bmsk, 0, bw * bh);
                //int blur_type_;
                CppSrc.BlurringUseMask(src, msk, dst, bw, bh, (int)blur_type_, kernel_size, stride, threshold);

                _dst = new Mat(bh, bw, MatType.CV_8UC1, dst);
                DelImg(msk);
                //DelImg(dst);
            }
            return _dst;
        }

        public static int GetBytePP(TPixFmtCs pf)
        {
            int bytepp = 1;
            if (pf == TPixFmtCs.Gray8) bytepp = 1;
            if (pf == TPixFmtCs.Rgb32) bytepp = 4;
            return bytepp;
        }
        public static IntPtr NewImg(int bw, int bh, TPixFmtCs pf)
        {
            //IntPtr buf = hlib.New64_A16(bw * bh * GetBytePP(pf));
            IntPtr buf = hlib.New32(bw * bh * GetBytePP(pf));
            if (buf == IntPtr.Zero) return IntPtr.Zero;
            hlib.MemSet(buf, 0, (long)bw * bh * GetBytePP(pf));
            return buf;
        }
        public static void DelImg(IntPtr buf)
        {
            hlib.Delete32(buf);
        }


         /*
        private static byte BlurringAvg(Mat src, Mat msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride)
        {
            byte dst = 0;
            int kcount = 0;
            int sum_value = 0;

            for (int ky = ky1; ky <= ky2; ky += stride)
            {
                if (ky < 0 || ky >= bh) continue;
                for (int kx = kx1; kx <= kx2; kx += stride)
                {
                    if (kx < 0 || kx >= bw) continue;

                    if (msk.At<byte>(kx, ky) == 0)
                    {
                        kcount++;
                        sum_value += src.At<byte>(kx, ky);
                    }
                }
            }
            if (kcount == 0) dst = 0;
            else dst = (byte)(sum_value / kcount);
            return dst;
        }
        private static byte BlurringMax(Mat src, Mat msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride)
        {
            byte dst = 0;

            for (int ky = ky1; ky <= ky2; ky += stride)
            {
                if (ky < 0 || ky + half_kernel_size > bh - 1) continue;
                for (int kx = kx1; kx <= kx2; kx += stride)
                {
                    if (kx < 0 || kx + half_kernel_size > bw - 1) continue;

                    if (msk.At<byte>(kx, ky) == 0)
                    {
                        byte b = src.At<byte>(kx, ky);
                        if (b > dst) dst = b;
                    }
                }
            }
            return dst;
        }
        private static byte BlurringStddev(Mat src, Mat msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride)
        {
            byte dst = 0;
            List<byte> data = new List<byte>();

            for (int ky = ky1; ky <= ky2; ky += stride)
            {
                if (ky < 0 || ky + half_kernel_size > bh - 1) continue;
                for (int kx = kx1; kx <= kx2; kx += stride)
                {
                    if (kx < 0 || kx + half_kernel_size > bw - 1) continue;

                    if (msk.At<byte>(kx, ky) == 0)
                    {
                        data.Add(src.At<byte>(kx, ky));
                    }
                }
            }
            if (data.Count == 0) return 0;

            double avg = 0, std = 0;
            for (int i = 0; i < data.Count; i++)
            {
                avg += data[i];
            }
            avg /= data.Count;
            for (int i = 0; i < data.Count; i++)
            {
                double delta = avg - data[i];
                std += delta * delta;
            }
            std /= data.Count - 1;
            dst = (byte)std;
            return dst;
        }
        private static byte BlurringAvgPlusStddev(Mat src, Mat msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride)
        {
            byte dst = 0;
            List<byte> data = new List<byte>();

            for (int ky = ky1; ky <= ky2; ky += stride)
            {
                if (ky < 0 || ky + half_kernel_size > bh - 1) continue;
                for (int kx = kx1; kx <= kx2; kx += stride)
                {
                    if (kx < 0 || kx + half_kernel_size > bw - 1) continue;

                    if (msk.At<byte>(kx, ky) == 0)
                    {
                        data.Add(src.At<byte>(kx, ky));
                    }
                }
            }
            if (data.Count == 0) return 0;

            double avg = 0, std = 0;
            for (int i = 0; i < data.Count; i++)
            {
                avg += data[i];
            }
            avg /= data.Count;
            for (int i = 0; i < data.Count; i++)
            {
                double delta = avg - data[i];
                std += delta * delta;
            }
            std /= data.Count - 1;
            dst = (byte)(avg + std);
            return dst;
        }
          * */

    }

}
