#define _CRT_SECURE_NO_WARNINGS
#include "Blurring.h"

unsigned char Data[MAX_DATA];

extern "C" __declspec(dllexport) void BlurringUseMask(unsigned char *src, unsigned char *msk, unsigned char *dst, int bw, int bh, int blur_type_, int kernel_size, int stride, int threshold) {
	printf("start\n");
	printf("%d\n", bw*bh);
	//memset(dst, 0, bw*bh);

	
	if (kernel_size <= 0 || kernel_size >= bw || kernel_size >= bh){
		return;
	}
	if (stride < 1 || stride >= bw || stride >= bh){
		return;
	}
	printf("스레쉬홀드 시작\n");
	Thresholding(src, msk, bw, bh, threshold);
	printf("스레쉬홀드 완료\n");
	int half_kernel_size = kernel_size / 2;
	int x1 = half_kernel_size;
	int y1 = half_kernel_size;
	int x2 = bw - half_kernel_size;
	int y2 = bh - half_kernel_size;
	TBlurType blur_type;
	switch (blur_type_)
	{
	case 0:
		blur_type = BLUR_AVG;
		break;
	case 1:
		blur_type = BLUR_MAX;
		break;
	case 2:
		blur_type = BLUR_STDDEV;
		break;
	case 3:
		blur_type = BLUR_AVG_PLUS_STDDEV;
		break;
	default:
		blur_type = BLUR_AVG;
		break;
	}
	printf("블러링 시작\n");
	
	for (int y = y1; y<y2; y++){
		if (y<0 || y >= bh) continue;
		unsigned char *d = &dst[(y*bw) + x1];
		for (int x = x1; x<x2; x++, d++){
			if (x<0 || x >= bw) continue;
			if (msk[(y*bw) + x] != 0) continue;

			int kx1 = x - half_kernel_size;
			int ky1 = y - half_kernel_size;
			int kx2 = x + half_kernel_size;
			int ky2 = y + half_kernel_size;

			if (blur_type == BLUR_AVG) {
				*d = (unsigned char)BlurringAvg(src, msk, bw, bh, kx1, ky1, kx2, ky2, half_kernel_size, stride);
			}
			else if (blur_type == BLUR_MAX){
				*d = (unsigned char)BlurringMax(src, msk, bw, bh, kx1, ky1, kx2, ky2, half_kernel_size, stride);
			}
			else if (blur_type == BLUR_STDDEV){
				*d = (unsigned char)BlurringStddev(src, msk, bw, bh, kx1, ky1, kx2, ky2, half_kernel_size, stride);
			}
			else if (blur_type == BLUR_AVG_PLUS_STDDEV){
				*d = (unsigned char)BlurringAvgPlusStddev(src, msk, bw, bh, kx1, ky1, kx2, ky2, half_kernel_size, stride);
			}
		}
	}
	printf("블러링 완료\n");

	for (int y = 0; y < bh; y++){
		for (int x = 0; x < bw; x++){
			if (msk[(y*bw) + x] != 0){
				dst[(y*bw) + x] = src[(y*bw) + x];
			}
		}
	}
	printf("이미지 더하기 완료\n");
}

extern "C" __declspec(dllexport) void AddConstantUseMask(unsigned char *src, unsigned char *msk, int constant, int bw, int bh){
	for (int i = 0; i < bh * bw; i++){
		if (msk[i] == 0){
			src[i] += constant;
		}
	}

}

unsigned char BlurringAvg(unsigned char* src, unsigned char* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride){
	int kcount = 0;
	int sum_value = 0;
	for (int ky = ky1; ky <= ky2; ky += stride){
		if (ky < 0 || ky >= bh) continue;
		unsigned char *m = &msk[ky*bw + kx1];
		unsigned char *p = &src[ky*bw + kx1];

		for (int kx = kx1; kx <= kx2; kx += stride, m += stride, p += stride){
			if (kx < 0 || kx >= bw) continue;

			if (*m == 0){
				kcount++;
				sum_value += *p;
			}
		}
	}
	if (kcount == 0) return 0;
	else return (unsigned char)(sum_value / kcount);
}
unsigned char BlurringMax(unsigned char* src, unsigned char* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride){
	int max_value = 0;
	for (int ky = ky1; ky <= ky2; ky += stride){
		if (ky < 0 || ky >= bh) continue;
		unsigned char *m = &msk[ky*bw + kx1];
		unsigned char *p = &src[ky*bw + kx1];

		for (int kx = kx1; kx <= kx2; kx += stride, m += stride, p += stride){
			if (kx < 0 || kx >= bw) continue;

			if (*m == 0){
				unsigned char b = *p;
				if (b>max_value) max_value = b;
			}
		}
	}
	
	return max_value;
}
unsigned char BlurringStddev(unsigned char* src, unsigned char* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride){
	int num = 0;
	memset(Data, 0, MAX_DATA);
	for (int ky = ky1; ky <= ky2; ky += stride){
		if (ky < 0 || ky >= bh) continue;
		unsigned char *m = &msk[ky*bw + kx1];
		unsigned char *p = &src[ky*bw + kx1];

		for (int kx = kx1; kx <= kx2; kx += stride, m += stride, p += stride){
			if (kx < 0 || kx >= bw) continue;

			if (*m == 0){
				Data[num++] = *p;
			}
		}
	}
	if (num == 0) return 0;
	double avg = 0, std = 0;
	for (int i = 0; i < num; i++)
	{
		avg += Data[i];
	}
	avg /= num;
	for (int i = 0; i < num; i++)
	{
		double delta = avg - Data[i];
		std += delta * delta;
	}
	std /= num - 1;
	if (std>0.0000000001) std = sqrt(std);
	return (unsigned char)std;
}
unsigned char BlurringAvgPlusStddev(unsigned char* src, unsigned char* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride){
	int num = 0;
	memset(Data, 0, MAX_DATA);
	for (int ky = ky1; ky <= ky2; ky += stride){
		if (ky < 0 || ky >= bh) continue;
		unsigned char *m = &msk[ky*bw + kx1];
		unsigned char *p = &src[ky*bw + kx1];

		for (int kx = kx1; kx <= kx2; kx += stride, m += stride, p += stride){
			if (kx < 0 || kx >= bw) continue;

			if (*m == 0){
				Data[num++] = *p;
			}
		}
	}
	if (num == 0) return 0;
	double avg = 0, std = 0;
	for (int i = 0; i < num; i++)
	{
		avg += Data[i];
	}
	avg /= num;
	for (int i = 0; i < num; i++)
	{
		double delta = avg - Data[i];
		std += delta * delta;
	}
	std /= num - 1;
	if (std>0.0000000001) std = sqrt(std);
	return (unsigned char)(avg + std);
}

void Thresholding(unsigned char *src, unsigned char *dst, int bw, int bh, int thr){
	printf("스레쉬홀드 함수\n");
	if(thr <= 0 || thr > 255) return;
	//memset(dst,0,bw*bh*sizeof(unsigned char));

	for(int i=0; i<bw*bh; i++){
	  if(src[i] >= thr){
		 dst[i] = 255;
	  } else {
		 dst[i] = 0;
	  }
   }

}