#pragma once

#include <iostream>
#include <Windows.h>

#define MAX_DATA 10000

enum TBlurType {
	BLUR_AVG,
	BLUR_MAX,
	BLUR_STDDEV,
	BLUR_AVG_PLUS_STDDEV
};

extern "C" __declspec(dllexport) void BlurringUseMask(BYTE *src, BYTE *msk, BYTE *dst, int bw, int bh, int blur_type, int kernel_size, int stride, int threshold);
extern "C" __declspec(dllexport) void AddConstantUseMask(unsigned char *src, unsigned char *msk, int constant, int bw, int bh);

BYTE BlurringAvg(BYTE* src, BYTE* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride);
BYTE BlurringMax(BYTE* src, BYTE* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride);
BYTE BlurringStddev(BYTE* src, BYTE* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride);
BYTE BlurringAvgPlusStddev(BYTE* src, BYTE* msk, int bw, int bh, int kx1, int ky1, int kx2, int ky2, int half_kernel_size, int stride);
void Thresholding(BYTE *src, BYTE *dst, int bw, int bh, int thr);