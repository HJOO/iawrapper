#include "BurringExe.h"
#include "opencv2\opencv.hpp"
#include "opencv2\opencv_modules.hpp"
#include "opencv2\core\core.hpp"
#include "opencv2\core\mat.hpp"
#include "opencv2\imgproc\imgproc.hpp"

using namespace cv;

void main(){
	Mat _src = imread("G:\\image\\g\\x20nwb01\\1F1B622QH1A001_0000_AQ_20181207113408.jpg", IMREAD_GRAYSCALE);
	//_src = Cv2.ImRead("G:\\image\\g\\x20nwb01\\1F1B622QH1A001_0000_AQ_20181207113408.jpg", ImreadModes.Grayscale);
	imshow("src", _src);
	int bw = _src.rows;
	int bh = _src.cols;
	BYTE *src = new BYTE[bw * bh];
	memcpy(src, _src.data, bw * bh);
	BYTE *msk = new BYTE[bw * bh];
	BYTE *dst = new BYTE[bw * bh];

	int blur_type = 1;
	int kernel_size =  3;
	int stride = 2;
	int threshold = 150; 
	
	BlurringUseMask(src, msk, dst, bw, bh, blur_type, kernel_size, stride, threshold);

	Mat _dst = Mat(bw, bh, CV_8UC1, dst).clone();
	Mat _msk = Mat(bw, bh, CV_8UC1, msk).clone();
	imshow("msk", _msk);
	imshow("dst", _dst);
	waitKey();
	return;
}