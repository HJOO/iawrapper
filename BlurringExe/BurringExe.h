#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#include <Windows.h>

extern "C" __declspec(dllimport) void BlurringUseMask(BYTE *src, BYTE *msk, BYTE *dst, int bw, int bh, int blur_type, int kernel_size, int stride, int threshold);