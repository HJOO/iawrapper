﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Hims.hLibCm;
using OpenCvSharp;

using libBase;

namespace IA_Blurring
{
    public partial class CppSrc
    {
        [DllImport("Blurring.dll")]
        public static extern void BlurringUseMask(IntPtr src, IntPtr msk, IntPtr dst, int bw, int bh, int blur_type, int kernel_size, int stride, int threshold);
    }
    public enum TPixFmtCs { Gray8, Rgb32, Num }

    public enum TBlurType
    {
        [Description("Average")]
        BLUR_AVG,
        [Description("Max")]
        BLUR_MAX,
        [Description("Stacdard Devation")]
        BLUR_STDDEV,
        [Description("Avg + Stddev")]
        BLUR_AVG_PLUS_STDDEV,
    }

    public class Blur_usingMaskImage : libBase.BaseModule<Mat, Mat>
    {
        TBlurType blur_type_ = TBlurType.BLUR_AVG;
        int stride = 1;
        int kernel_size = 3;
        int threshold = 0;

        public Blur_usingMaskImage()
        {
            Info.Category = libBase.FunctionCategory.All;
            Info.Title = "Mask Blurring";
            Info.Description = "마스크 이미지를 이용한 이미지 흐림";


        }
        //public override int NoOfInputs { get { return 2; } }
        // [TypeConverter(typeof(libBase.Common.brEnumConvertor))] --> enum text convertor
        [Description("Blur Type")]
        [TypeConverter(typeof(libBase.Common.brEnumConvertor))]
        public TBlurType blur_type
        {
            get
            {
                return blur_type_;
            }
            set
            {
                blur_type_ = value;
                Action();
            }
        }
        [Description("Stride")]
        public int Stride { get { return stride; } set { stride = value; Action(); } }
        [Description("Kernel Size")]
        public int Kernel_size { get { return kernel_size; } set { kernel_size = value; Action(); } }
        [Description("Threshold")]
        public int Threshold { get { return threshold; } set { threshold = value; Action(); } }


        protected override Mat Execute(Mat value)
        {
            if (value == null)
                return null;

            IntPtr src = value.Data;
            int bw = value.Width;
            int bh = value.Height;
            IntPtr msk = NewImg(bw, bh, TPixFmtCs.Gray8);
            IntPtr dst = NewImg(bw, bh, TPixFmtCs.Gray8);

            CppSrc.BlurringUseMask(src, msk, dst, value.Width, value.Height, (int)blur_type_, kernel_size, stride, threshold);

            Mat result = new Mat(bh, bw, MatType.CV_8UC1, dst);
            DelImg(msk);

            return result;
        }




        public static int GetBytePP(TPixFmtCs pf)
        {
            int bytepp = 1;
            if (pf == TPixFmtCs.Gray8) bytepp = 1;
            if (pf == TPixFmtCs.Rgb32) bytepp = 4;
            return bytepp;
        }
        public static IntPtr NewImg(int bw, int bh, TPixFmtCs pf)
        {
            //IntPtr buf = hlib.New64_A16(bw * bh * GetBytePP(pf));
            IntPtr buf = hlib.New32(bw * bh * GetBytePP(pf));
            if (buf == IntPtr.Zero) return IntPtr.Zero;
            hlib.MemSet(buf, 0, (long)bw * bh * GetBytePP(pf));
            return buf;
        }
        public static void DelImg(IntPtr buf)
        {
            hlib.Delete32(buf);
        }

    }
}
